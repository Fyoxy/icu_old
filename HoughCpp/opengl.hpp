#include <GL/glut.h>

/*************************OPENGL*********************************/
void display (void){
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0,1.0,1.0);
	glFlush();
	
}

void drawRedSquare(int x, int y) {
	y = 600-y;
	
	glPointSize(10);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_POINTS);
	glVertex2f(x , y);
	glEnd();
	glutSwapBuffers();
}

void drawBlueSquare(int x, int y) {
	y = 600-y;
	
	glPointSize(10);
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_POINTS);
	glVertex2f(x , y);
	glEnd();
	glutSwapBuffers();
}

void mouse(int bin, int state , int x , int y) {
	if(bin == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
        if (y > 400) drawRedSquare(x,y);
        else drawBlueSquare(x,y);
    }
}

void init (void)
{
	/* select clearing (background) color */
	glClearColor (1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	
	glViewport( 0,0, 800, 600 );
	glMatrixMode( GL_PROJECTION );
	glOrtho( 0.0, 800.0, 0.0, 600.0, 1.0, -1.0 );
	
	/* initialize viewing values */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/************************OPENGL END******************************/