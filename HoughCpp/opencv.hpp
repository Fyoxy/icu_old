#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opengl.hpp"
#include "iostream"

using namespace cv;
using namespace std;

void openCV()
{
    // OpenCV

    VideoCapture cap(0);
    //if(!cap.isOpened()) return -1;

    Mat frame, edited;
    //frame = imread("pic.png");

    // Infinite loop
    for(;;)
    {
        cout << "Draw!" << endl;
        drawRedSquare(300, 400);
        cap >> frame;
        // Conveting video to HSV
        cvtColor(frame, edited, COLOR_BGR2HSV);

        std::vector<Mat> channels;

        // Splitting HSV frame into 3 channels
        split(edited, channels);
        // Getting the HUE channel from the HSV image
        Mat H = channels[1];
        
        /* Blur for canny accuracy
         * Blur seems to have a destructive effect if removed
         */
        //GaussianBlur(H, H, Size(7,7), 1.5, 1.5);

		//bilateralFilter(H, H, 9, 75, 75, BORDER_DEFAULT);

        Mat mask1,mask2;
	    // Creating masks to detect the upper and lower color (currently white).
	    inRange(H, Scalar(0, 0, 0), Scalar(0, 0, 0), mask1);
	    inRange(H, Scalar(255, 255, 255), Scalar(255, 255, 255), mask2);
        mask1 = mask1 + mask2;

        Canny(mask1, mask1, 200, 300);

        // extract contours of the canny image:
        std::vector<std::vector<cv::Point> > contoursH;
        std::vector<cv::Vec4i> hierarchyH;
        cv::findContours(mask1, contoursH, hierarchyH, RETR_TREE , CHAIN_APPROX_SIMPLE);

        for (int i = 0; i < contoursH.size(); i++)
        {
            //cout << "contoursH: " << contoursH.size() << endl;
            drawContours(frame, contoursH, i, Scalar(0,0,255), 2, 8, hierarchyH, 0);
        }

        imshow("hsv", mask1);
        imshow("draw", frame);
        if(waitKey(30) >= 0) break;
    }
}