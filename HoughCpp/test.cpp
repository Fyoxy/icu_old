#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "iostream"


using namespace cv;
using namespace std;


int main(int, char**)
{
    Mat frame;
    namedWindow("video", 1);
    VideoCapture cap("http://192.168.1.88/html/cam_pic_new.php");
    if(!cap.isOpened())
    {
        cout<<"Camera not found"<<endl;
        getchar();
        return -1;
    }
    while ( cap.isOpened() )
    {
        cap >> frame;
        if(frame.empty()) break;

        imshow("video", frame);
        if(waitKey(5) >= 0) break;
    }   
    return 0;
}