#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/highgui/highgui_c.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <numeric>

using namespace std;
using namespace cv;


void Dilation( int, void* );
void MyFilledCircle( Mat img, Point center );
double VectorAvg( std::vector<int> const& v );

Mat src;

int main(int argc,char** argv){

	// Define manual driving control
	bool manual = true;

	// Define video capture
	VideoCapture cap("output.avi");

	while ( cap.isOpened() )
    {
		// Send VideoCapture to src Mat
		cap >> src;
        if(src.empty()) break;
		
		Mat crop;
		Rect crop_region(0, 120, src.size().width, 120);

		crop=src(crop_region);

		// Clahe equalization
		// READ RGB color image and convert it to Lab
    	Mat lab_image;
    	cvtColor(crop, lab_image, CV_BGR2Lab);
		
    	// Extract the L channel
    	vector<Mat> lab_planes(3);
    	split(lab_image, lab_planes);  // now we have the L image in lab_planes[0]

    	// apply the CLAHE algorithm to the L channel
    	Ptr<CLAHE> clahe = createCLAHE();
    	clahe->setClipLimit(3);
		clahe->setTilesGridSize( Size (8,8) );
    	Mat dst;
    	clahe->apply(lab_planes[0], dst);

    	// Merge the the color planes back into an Lab image
    	dst.copyTo(lab_planes[0]);
    	cv::merge(lab_planes, lab_image);

   		// convert back to RGB
   		Mat image_clahe;
   		cvtColor(lab_image, image_clahe, CV_Lab2BGR);

		// Convert to HSV
		Mat hsv;
		cvtColor(image_clahe, hsv, CV_BGR2HSV);

		// Warp image
		/*
		vector< Point2f> roi_corners;
		vector< Point2f> midpoints(4);
		vector< Point2f> dst_corners(4);

		dst_corners[0].x = 0;
        dst_corners[0].y = 130;
		dst_corners[1].x = 480;
        dst_corners[1].y = 240;
		dst_corners[2].x = 0;
        dst_corners[2].y = 240;
		dst_corners[3].x = 480;
        dst_corners[3].y = 240;

		Size warped_image_size = Size(cvRound(dst_corners[2].x), cvRound(dst_corners[2].y));

		roi_corners.push_back(Point2f( (float)(src.cols), (float)(src.rows) ));
    	roi_corners.push_back(Point2f( (float)(src.cols), (float)(src.rows) ));
    	roi_corners.push_back(Point2f( (float)(src.cols), (float)(src.rows) ));
    	roi_corners.push_back(Point2f( (float)(src.cols), (float)(src.rows) ));

		Mat M = getPerspectiveTransform(roi_corners, dst_corners);

		Mat warped_image;
        warpPerspective(src, warped_image, M, warped_image_size); // do perspective transformation

		imshow("warp", warped_image);
		*/

		// Detect floor
		Mat frame_threshold;
		inRange(hsv, Scalar(37, 0, 0), Scalar(179, 255, 255), frame_threshold);

		// Get source image height and width
		int height = src.size().height;
		int width = src.size().width;

		// Sum image intensity values by columns
		Mat histogramValues;
        reduce( frame_threshold, histogramValues, 0, CV_REDUCE_SUM, CV_32F );
        
		// Find min and max values
		double min, max;
		minMaxIdx( histogramValues, &min, &max );
		min = 0.8 * max;

		// Filter noise
		vector<int> arr;
		size_t c = 0;

		for(size_t i = 0; i < histogramValues.cols; i++)
		{
			if (histogramValues.at<float>(0, i) >= min) 
			{
				arr.push_back( i );
				c++;
			}
		}

		// Find the average value
        double average = VectorAvg(arr);

		Mat histogram(height,width,CV_8UC3,Scalar(0,0,0));

		double intensity;

		for (int i = 0; i < width; i++)
		{
			intensity = histogramValues.at<float>(0, i);
			line( histogram, Point(i, height), Point(i, height - ( ( int ) intensity / 255 ) ), Scalar( 140, 0, 255 ), 1, LINE_8 );
		}

		MyFilledCircle(histogram, Point((int)average, height));

		Mat result(height, width, CV_8UC3, cv::Scalar(0,255,0));
		Mat thefinal;
		addWeighted(src, 1, histogram, 1, 0, thefinal);
		//addWeighted(src1, alpha, src2, beta, 0.0, dst);
		
		imshow("source", src);
		imshow("Area", thefinal);
		imshow("histogram", histogram);
        imshow("hsv", hsv);
		imshow("detectedFloor", frame_threshold);
    }   

	destroyAllWindows();

    return 0;
} 

void MyFilledCircle( Mat img, Point center )
{
	/* Source image */
	/* Center point */
	/* Circle size  */
	/* Source image */
	/* Circle color */
	circle( img,
    	center,
    	20,
    	Scalar( 0, 255, 255 ),
    	FILLED,
    	LINE_8 );
}

double VectorAvg(std::vector<int> const& v) {
    return 1.0 * std::accumulate(v.begin(), v.end(), 0LL) / v.size();
}