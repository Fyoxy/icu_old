import cv2
import sys
import numpy as np
import time

import utils
import trackbars
#from driving import ReleaseMotors


start = time.time()

def printMenu():
    print(30 * "-", "ICU Menu", 30 * "-")
    print("1. Start regular")
    print("2. Start local")
    print("3. Calibrate floor")
    print(70 * "-")

def runDetection(cap):

    import driving

    driving.SetupMotors()

    manual = True
    angle = 1500

    while True:

        success, img = cap.read()

        img = cv2.flip(img, -1)
        img = cv2.resize(img, (480, 240), interpolation = cv2.INTER_LINEAR)
        
        # convert to gray
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # threshold grayscale image to extract glare
        mask = cv2.threshold(gray, 120, 255, cv2.THRESH_BINARY)[1]
        
        mask = cv2.bitwise_not(mask)
        result = cv2.bitwise_or(img, img, mask=mask)

        curve = utils.detectFloor(result, display = 2)
        print(curve)

        key = cv2.waitKey(1) & 0xFF

        # Switch to manual driving
        if (key == ord('q')):
            break
        elif (key == ord('p')):
            manual = True
        elif (key == ord('o')):
            manual = False

        if (manual):
            angle = driving.ManualControl(key, angle)
        else:
            angle = driving.AutomaticControl(curve, angle)

def runLocalDetection(cap):

    while True:

        success, img = cap.read()

        if (success == False):
            break

        img = cv2.resize(img, (480, 240), interpolation = cv2.INTER_LINEAR)

        # apply clahe
        lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
        l, a, b = cv2.split(lab)
        clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
        cl = clahe.apply(l)
        limg = cv2.merge((cl,a,b))
        final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)

        cv2.imshow('clahe', final)
        
        # convert to gray
        #gray = cv2.cvtColor(final, cv2.COLOR_BGR2GRAY)

        # threshold grayscale image to extract glare
        #mask = cv2.threshold(gray, 120, 255, cv2.THRESH_BINARY)[1]
        #
        #mask = cv2.bitwise_not(mask)
        #result = cv2.bitwise_or(img, img, mask=mask)

        curve = utils.detectFloor(final, display = 2)
        #print(curve)

        key = cv2.waitKey(1) & 0xFF

        if (key == ord('q')):
            break

        if (key == ord(' ')):
            cv2.waitKey(0) & 0xFF


try:
    if __name__ == '__main__':

        if (str(sys.argv[len(sys.argv) - 1]) == 'auto'):
            # 90 degrees on servo
            centerPulse = 1500

            # Toggle manual driving on and off
            manual = True

            angle = centerPulse

            cap = cv2.VideoCapture(0)

            # Set caputre width and height
            #cap.set(3, 480)
            #cap.set(4, 240)

            runDetection(cap)
        else:
            printMenu()
            selection = int(input("Enter option [1-3]: "))

        #initialTrackbarValues = [0, 112, 0, 240]
        initialTrackbarValues = [0, 130, 0, 240]
        trackbars.initializeTrackbars(initialTrackbarValues)
        
        print(selection)

        if selection == 1:
            cap = cv2.VideoCapture(0)
            runDetection(cap)
        elif selection == 2:
            cap = cv2.VideoCapture('../Rada/output.avi')
            runLocalDetection(cap)

finally:
    try:
        cap.release()
    except:
        print("No capture released!")
    try:
        ReleaseMotors()
    except:
        print("No motors released!")
    
    end = time.time()
    print("Time taken: ")
    print(end - start)
    print("Driving end")
