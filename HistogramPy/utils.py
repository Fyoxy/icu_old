import cv2
import numpy as np
import trackbars

curveList = []
averageVal = 10

def detectFloor(img, display = 0):

    imgCopy = img.copy()
    imgResult = img.copy()

    ### Detect the floor
    imgTresh = threshold(img)

    ### Warp the image
    h, w, c = img.shape
    points = trackbars.valTrackbars()

    imgWarp = warpImg(imgTresh, points, w, h)

    if display != 0:
        imgWarpPoints = drawPoints(imgCopy, points)

    ### Get the histogram
    if display != 0:
        middlePoint, imgHistogram = getHistogram(imgWarp, display = True, minPer = 0.5, region = 4)
        curveAveragePoint, imgHistogram = getHistogram(imgWarp, display = True, minPer = 0.9)
    else:
        middlePoint = getHistogram(imgWarp, display = False, minPer = 0.5, region = 4)
        curveAveragePoint = getHistogram(imgWarp, display = False, minPer = 0.9)

    ### Get the curvature
    rawCurve = curveAveragePoint - middlePoint


    ### Average curvature
    curveList.append(rawCurve)
    if len(curveList) > averageVal:
        curveList.pop(0)
    
    curve = int(sum(curveList) / len(curveList))


    ### Display
    if display != 0:
        imgInvWarp = warpImg(imgWarp, points, w, h, inverse = True)
        imgInvWarp = cv2.cvtColor(imgInvWarp, cv2.COLOR_GRAY2BGR)
        imgInvWarp[0:h // 3, 0:w] = 0, 0, 0
        imgLaneColor = np.zeros_like(img)
        imgLaneColor[:] = 0, 255, 0
        imgLaneColor = cv2.bitwise_and(imgInvWarp, imgLaneColor)
        imgResult = cv2.addWeighted(imgResult, 1, imgLaneColor, 1, 0)
        midY = 450
        cv2.putText(imgResult, str(curve), (w // 2 - 80, 85), cv2.FONT_HERSHEY_COMPLEX, 2, (255, 0, 255), 3)
        cv2.line(imgResult, (w // 2, midY), (w // 2 + (curve * 3), midY), (255, 0, 255), 5)
        cv2.line(imgResult, ((w // 2 + (curve * 3)), midY - 25), (w // 2 + (curve * 3), midY + 25), (0, 255, 0), 5)
        for x in range(-30, 30):
            w = w // 20
            cv2.line(imgResult, (w * x + int(curve // 50), midY - 10),
                     (w * x + int(curve // 50), midY + 10), (0, 0, 255), 2)
    if display == 2:
        imgStacked = stackImages(0.7, ([img, imgWarpPoints, imgWarp],
                                             [imgHistogram, imgLaneColor, imgResult]))
        cv2.imshow('ImageStack', imgStacked)
    elif display == 1:
        cv2.imshow('Result', imgResult)
 
    #### NORMALIZATION
    curve = curve / 1
    if curve > 100: curve == 100
    if curve < -100: curve == -100
 
    return curve


def threshold(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    
    #lowerFloor = np.array([8,10,39])
    #upperFloor = np.array([52,102,238])

    #lowerFloor = np.array([0,0,0])
    #upperFloor = np.array([179,181,95])

    #lowerFloor = np.array([0,0,0])
    #upperFloor = np.array([179,255,121])

    lowerFloor = np.array([37,0,0])
    upperFloor = np.array([179,255,255])

    maskFloor = cv2.inRange(hsv, lowerFloor, upperFloor)

    return maskFloor

def warpImg(img, points, w, h, inverse = False):
    pt1 = np.float32(points)
    pt2 = np.float32([[0, 0], [w, 0], [0, h], [w, h]])

    if inverse:
        matrix = cv2.getPerspectiveTransform(pt2, pt1)
    else:
        matrix = cv2.getPerspectiveTransform(pt1, pt2)


    imgWarp = cv2.warpPerspective(img, matrix, (w, h))

    return imgWarp

### Warping visual feedback draw image points
def drawPoints(img, points):
    for x in range(4):
        cv2.circle( img, 
                    (int(points[x][0]), 
                    int(points[x][1])), 
                    15, 
                    (0, 0, 255),
                    cv2.FILLED)

    return img

def getHistogram(img, minPer = 0.8, display = False, region = 1):

    if region == 1:
        histValues = np.sum(img, axis = 0)
    else:
        histValues = np.sum(img[img.shape[0] // region:,:], axis = 0)

    histogramValues = np.sum(img, axis = 0)

    #print(histogramValues)
    
    maxValue = np.max(histogramValues)
    minValue = minPer * maxValue

    indexArray = np.where(histogramValues >= minValue)
    basePoint = int(np.average(indexArray))

    #print(basePoint)
    
    if display:
        imgHistogram = np.zeros((img.shape[0], img.shape[1] , 3), np.uint8)

        for x, intensity in enumerate(histogramValues):
            cv2.line(   imgHistogram, 
                        (x, img.shape[0]), 
                        (x, img.shape[0] - int(round(intensity / 255 / region))),
                        (255, 0, 255),
                        1)
            cv2.circle( imgHistogram, 
                        (basePoint, img.shape[0]), 
                        20,
                        (0, 255, 255), 
                        cv2.FILLED)
        return basePoint, imgHistogram

    return basePoint



def stackImages(scale,imgArray):
    rows = len(imgArray)
    cols = len(imgArray[0])
    rowsAvailable = isinstance(imgArray[0], list)
    width = imgArray[0][0].shape[1]
    height = imgArray[0][0].shape[0]
    if rowsAvailable:
        for x in range ( 0, rows):
            for y in range(0, cols):
                if imgArray[x][y].shape[:2] == imgArray[0][0].shape [:2]:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (0, 0), None, scale, scale)
                else:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (imgArray[0][0].shape[1], imgArray[0][0].shape[0]), None, scale, scale)
                if len(imgArray[x][y].shape) == 2: imgArray[x][y]= cv2.cvtColor( imgArray[x][y], cv2.COLOR_GRAY2BGR)
        imageBlank = np.zeros((height, width, 3), np.uint8)
        hor = [imageBlank]*rows
        hor_con = [imageBlank]*rows
        for x in range(0, rows):
            hor[x] = np.hstack(imgArray[x])
        ver = np.vstack(hor)
    else:
        for x in range(0, rows):
            if imgArray[x].shape[:2] == imgArray[0].shape[:2]:
                imgArray[x] = cv2.resize(imgArray[x], (0, 0), None, scale, scale)
            else:
                imgArray[x] = cv2.resize(imgArray[x], (imgArray[0].shape[1], imgArray[0].shape[0]), None,scale, scale)
            if len(imgArray[x].shape) == 2: imgArray[x] = cv2.cvtColor(imgArray[x], cv2.COLOR_GRAY2BGR)
        hor= np.hstack(imgArray)
        ver = hor
    return ver
