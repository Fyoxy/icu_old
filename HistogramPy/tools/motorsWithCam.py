import cv2
import pigpio
import RPi.GPIO as GPIO

rev = 0

cap = cv2.VideoCapture(0)

GPIO.setmode(GPIO.BCM)
# In1 = aEnable
In2, In1, servo = 13, 12, 2
bPhase, bEna = 21, 20

# SERVO
pi = pigpio.pi()
pi.set_mode(servo, pigpio.OUTPUT)
pi.set_PWM_frequency(servo, 50)

angle = 1400

# Motors
GPIO.setup(In1, GPIO.OUT)
GPIO.setup(In2, GPIO.OUT)
GPIO.setup(bPhase, GPIO.OUT)
GPIO.setup(bEna, GPIO.OUT)


pwm = GPIO.PWM(In1, 100)
pwm.start(0)

pwmB = GPIO.PWM(bEna, 100)
pwmB.start(0)

direction = 420

def MotorsForward():
    print("Forwards!")
    GPIO.output(In2, GPIO.LOW)
    GPIO.output(bPhase, GPIO.LOW)

def MotorsBackward():
    print("Backwards!")
    GPIO.output(In2, GPIO.HIGH)
    GPIO.output(bPhase, GPIO.HIGH)

def SetAngle(angle):
    pi.set_servo_pulsewidth(servo, angle)



try:

    SetAngle(angle)

    while(1):
        success, img = cap.read()
        img = cv2.flip(img, -1)
        cv2.imshow('img',img)
        key = cv2.waitKey(1) & 0xFF
        
        if (key == ord('q')):
            break
        elif (key == ord('s')):
            direction = 1
            MotorsForward()
            pwm.ChangeDutyCycle(40)
            pwmB.ChangeDutyCycle(40)
        elif (key == ord('w')):
            direction = 0
            MotorsBackward()
            pwm.ChangeDutyCycle(50)
            pwmB.ChangeDutyCycle(50)
        elif (key == ord(' ')):
            print(direction)
            if (direction == 1):
                print("Forward Breaking")
                pwm.ChangeDutyCycle(0)
                pwmB.ChangeDutyCycle(0)
            elif (direction == 0):
                print("Backwards Breaking")
                pwm.ChangeDutyCycle(100)
                pwmB.ChangeDutyCycle(100)
        elif (key == ord('d')):
            if (angle < 1100):
                print("At minimum angle!")
                angle = 1100
            else:
                angle -= 50
                SetAngle(angle)
                print(angle)
        elif (key == ord('a')):
            if (angle > 1600):
                print("At maximum angle!")
                angle = 1600
            else:
                angle += 50
                SetAngle(angle)
                print(angle)
        elif (key == ord('r')):
            angle = 1400
            SetAngle(angle)
            print(angle)
            
            
finally:
    cap.release()
    try:
        pwm.stop()
    except Exception:
        pass

    try:
        pwmB.stop()
    except Exception:
        pass

    GPIO.cleanup()

    pi.set_PWM_dutycycle(servo, 0)
    pi.set_PWM_frequency(servo, 0)