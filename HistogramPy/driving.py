# First run sudo pigpiod

import cv2
import pigpio
import RPi.GPIO as GPIO


#--- Define pins ---#
aPhase, aEna, servo = 26, 19, 2
bPhase, bEna = 20, 16

#--- Set pi pin mode ---#
GPIO.setmode(GPIO.BCM)

#--- Setup motor pins ---#
GPIO.setup(aEna, GPIO.OUT)
GPIO.setup(aPhase, GPIO.OUT)
GPIO.setup(bPhase, GPIO.OUT)
GPIO.setup(bEna, GPIO.OUT)

#--- Define PWM pins ---#
pwmA = GPIO.PWM(aEna, 100)
pwmB = GPIO.PWM(bEna, 100) # Enable pin and frequency for the motors

#--- Define motor speeds ---#
forwardSpeed  = 80 # Higher the number slower the speed
backwardSpeed = 20 # Higher the number faster the speed

#--- Define pigpio variable ---#
pi = pigpio.pi()


def SetupMotors():
    #--- Servo pigpio setup ---#
    pi.set_mode(servo, pigpio.OUTPUT)
    pi.set_PWM_frequency(servo, 50)

    #--- Start PWM signal to motors ---#
    #pwmA.start(0)
    if not GPIO.input(aPhase):
        pwmA.start(0) # PWM signal to start the motors while forwarding
    elif GPIO.input(aPhase):
        pwmA.start(100) # PWM signal to start the motors while reversing


    if not GPIO.input(bPhase):
        pwmB.start(0) # PWM signal to start the motors while forwarding
    elif GPIO.input(bPhase):
        pwmB.start(100) # PWM signal to start the motors while reversing

    #--- Set servo angle to 90 degrees ---#
    SetAngle(0)

def ReleaseMotors():
    #--- Stop PWM signal to motors ---#
    try:
        pwmA.stop()
    except Exception:
        pass

    try:
        pwmB.stop()
    except Exception:
        pass

    #--- Cleanup motor pins ---#
    GPIO.cleanup()

    # ADDITIONAL CLEANUP NEEDED HERE

    #--- Cleanup servo ---#
    pi.set_PWM_dutycycle(servo, 0)
    pi.set_PWM_frequency(servo, 0)

#--- Function SetAngle - Sets the desired angle (pulsewidth) for the servo ---#
#--- Arguments:                                                            ---#
#---    direction -> the direction where to turn, 1 for right, 2 for left  ---#
#---    angle     -> the servo angle (pulsewidth) to set for the servo     ---#
#--- Returns:                                                              ---#
#       angle     -> the set angle for the servo                           ---#

def SetAngle(direction = 0, angle = 1500):
    #--- Set servo min and max angles (pulse width) ---#
    maxPulse = 1900
    minPulse = 1100

    print(angle)

    if (direction == 1):
        if (angle <= minPulse):
            print("At minimum angle!")
        else:
            angle -= 25 # Reduce angle by 25 for smooth transition
            pi.set_servo_pulsewidth(servo, angle) # Set the pulsewidth

    elif (direction == 2):
        if (angle >= maxPulse):
            print("At maximum angle!")
        else:
            angle += 25 # Increase angle by 25 for smooth transition
            pi.set_servo_pulsewidth(servo, angle) # Set the pulsewidth

    else:
        pi.set_servo_pulsewidth(servo, angle) # Set center pulsewidth

    return angle # Return the newly defined angle


def ManualControl(key, angle):
    
    #angle = centerPulse # angle that is used for steering

    # Drive forward
    if (key == ord('w')):
        # Check if phase on LOW (backward) or HIGH (forward)
        if not GPIO.input(aPhase): # If LOW set to HIGH
            GPIO.output(aPhase, GPIO.HIGH)
        if not GPIO.input(bPhase): # If LOW set to HIGH
            GPIO.output(bPhase, GPIO.HIGH)
        # Backward at 50%
        pwmA.ChangeDutyCycle(forwardSpeed)
        pwmB.ChangeDutyCycle(forwardSpeed)
    # Drive backward
    elif (key == ord('s')):
        # Check if phase on LOW (backward) or HIGH (forward)
        if GPIO.input(aPhase): # If HIGH set to LOW
            GPIO.output(aPhase, GPIO.LOW)
        if GPIO.input(bPhase): # If HIGH set to LOW
            GPIO.output(bPhase, GPIO.LOW)
        
        # Backwards at 50%
        pwmA.ChangeDutyCycle(backwardSpeed)
        pwmB.ChangeDutyCycle(backwardSpeed)
    # Breaking
    elif (key == ord(' ')):
        # Break on reverse
        if GPIO.input(aPhase):
            pwmA.ChangeDutyCycle(100)
        else:
            pwmA.ChangeDutyCycle(0)
        # Break on reverse
        if GPIO.input(bPhase):
            pwmB.ChangeDutyCycle(100)
        # Break on forward
        else:
            pwmB.ChangeDutyCycle(0)
    
    # Turn right
    elif (key == ord('d')):
        angle = SetAngle(1, angle)
        
    # Turn left
    elif (key == ord('a')):
        angle = SetAngle(2, angle)
    # Reset servo angle to 90
    elif (key == ord('r')):
        angle = SetAngle(0)
    

    return angle

def AutomaticControl(curve, angle):
    #-- Set servo min and max angles (pulse width) ---#
    maxPulse = 1900
    minPulse = 1100

    # Check if phase on LOW (backward) or HIGH (forward)
    if not GPIO.input(aPhase): # If LOW set to HIGH
        GPIO.output(aPhase, GPIO.HIGH)
    if not GPIO.input(bPhase): # If LOW set to HIGH
        GPIO.output(bPhase, GPIO.HIGH)
    # Forward at 50%
    pwmA.ChangeDutyCycle(90)
    pwmB.ChangeDutyCycle(90)

    curve = 4 * curve

    if (curve > 0):
        angle = 1500 - curve
    elif (curve < 0):
        angle = 1500 - curve
        print(angle)
    else:
        angle = 1500

    if (angle < 1100):
        angle = 1100
    elif (angle > 1900):
        angle = 1900

    pi.set_servo_pulsewidth(servo, angle) # Set the pulsewidth


    return angle # Return the newly defined angle

if __name__ == "__main__":

    cap = cv2.VideoCapture(0)

    angle = 1500 # 90 degrees on servo

    try:
        SetupMotors()

        while(1):
            success, img = cap.read()
            img = cv2.flip(img, -1)
            img = cv2.resize(img, (480, 240))

            cv2.imshow('img',img)
            key = cv2.waitKey(1) & 0xFF

            if (key == ord('q')):
                break
            else:
                angle = ManualControl(key, angle)

    finally:
        cap.release()
        ReleaseMotors()
