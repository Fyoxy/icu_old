import socket

ipaddr = '192.168.1.88'
port = 1337

s = socket.socket()          
print "Socket successfully created"

s.bind((ipaddr, port))
print ("Socket binded to %s / %s" %(ipaddr), %(port)) 

s.listen(5)
print "Pi socket is listening"            
     
while True: 

   c, addr = s.accept()      
   print 'Got connection from', addr 

   c.send('Thank you for connecting') 

   c.close() 